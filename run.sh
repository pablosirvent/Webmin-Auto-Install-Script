#!/bin/bash
# This script installs webmin on Ubuntu
# created by Pablo Sirvent Jiménez on https://www.pablosirvent.net
WORK_DIR=`mktemp -d -p "$DIR_R"`
function cleanup {
  rm -rf "$WORK_DIR"
}
cd $WORK_DIR
echo "/////////////////////////////////////////////////////////////////////////////////////////"
echo "///////////////////////////Created by Pablo Sirvent//////////////////////////////////////"
echo "///////////////////////////https://pablosirvent.net//////////////////////////////////////"
echo "////////////////////////////  Webmin AutoInstall ////////////////////////////////////////"
echo "/////////////////////////////////////////////////////////////////////////////////////////"
read -p "//////////////////////////Press any key to install///////////////////////////////////"
apt-get update && apt-get -y upgrade
clear
echo "///////////Now you need to press 'yes' in phpmyadmin to continue///////////////////////"
read -p "//////////////////////////Press any key to continue///////////////////////////////////"
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python
clear
read -p "Do you want to install the latest webmin relase (1.831)? <y/N> " prompt
if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
then
   wget https://netix.dl.sourceforge.net/project/webadmin/webmin/1.831/webmin_1.831_all.deb
   dpkg --install webmin_1.831_all.deb
else
   read -p "Do you want to install a custom webmin release? <y/N> " prompt
        if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
        then
           echo "Enter the URL to download webmin (you can find it on sourceforge)"
           read URL
           wget "${URL}"
           dpkg --install "${URL##*/}"
        else
          exit 0
        fi
fi
trap cleanup EXIT
